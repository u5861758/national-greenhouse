import dash
import dash_html_components as html
import plotly.graph_objects as go
import dash_core_components as dcc
from dash.dependencies import Input, Output
import pandas as pd

app = dash.Dash()

url_Controlling_corporations_registered = 'https://www.cleanenergyregulator.gov.au/DocumentAssets/Documents/National%20Greenhouse%20and%20Energy%20Register%20controlling%20corporations%202021-22.csv'
url_Reporting_Transfer = 'https://www.cleanenergyregulator.gov.au/DocumentAssets/Documents/National%20Greenhouse%20and%20Energy%20Register%20RTC%20holders%202021-22.csv'
url_Controlling_corporations_deregistered = 'https://www.cleanenergyregulator.gov.au/DocumentAssets/Documents/National%20Greenhouse%20and%20Energy%20Register%20corporations%20deregistered%202021-22.csv'
url_Responsible_emitters = 'https://www.cleanenergyregulator.gov.au/DocumentAssets/Documents/National%20Greenhouse%20and%20Energy%20Register%20responsible%20emitters%202021-22.csv'


app.layout = html.Div(id='parent', children=[
    html.H1(id='H1', children='National Greenhouse and Energy Register 2021-22', style={'textAlign': 'center', \
                                                                      'marginTop': 40, 'marginBottom': 40}),

    dcc.Dropdown(id='dropdown',
                 options=[
                     {'label': 'Controlling corporations registered under the NGER Act (including Reporting Transfer Certificate holders) for the 2021-22 reporting year', 'value': url_Controlling_corporations_registered},
                     {'label': 'Reporting Transfer Certificate holders for the 2021-22 reporting year', 'value': url_Reporting_Transfer},
                     {'label': 'Controlling corporations that were deregistered and not required to report under the NGER Act for the 2021-22 reporting year', 'value': url_Controlling_corporations_deregistered},
                     {'label': 'Responsible emitters (Safeguard Mechanism) registered under the NGER Act for the 2021-22 reporting year', 'value': url_Responsible_emitters},
                 ],
                 value=url_Controlling_corporations_registered),
    dcc.Graph(id='table_plot')
])


@app.callback(Output(component_id='table_plot', component_property='figure'),
              [Input(component_id='dropdown', component_property='value')])



def graph_update(dropdown_value):
    headerColor = 'grey'
    rowEvenColor = 'lightgrey'
    rowOddColor = 'white'

    if dropdown_value == url_Controlling_corporations_registered:
        df = pd.read_csv(dropdown_value, skiprows=3) #deal with special case
    else:
        df = pd.read_csv(dropdown_value)


    fig = go.Figure(data=[go.Table(
        header=dict(values=list(df.columns),
                    line_color='darkslategray',
                    fill_color=headerColor,
                    align=['left', 'center'],
                    font=dict(color='white', size=12)),
        cells=dict(values=[df['Organisation name'], df['Identifying details']],
                   line_color='darkslategray',
                   fill_color=[[rowOddColor, rowEvenColor, rowOddColor, rowEvenColor] * 1000],
                   align=['left', 'center'],
                   font=dict(color='darkslategray', size=11)
                   ))
    ])

    return fig


if __name__ == '__main__':
    app.run_server()