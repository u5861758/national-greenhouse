# National Greenhouse

## Description
Under the National Greenhouse and Energy Reporting Act 2007 (NGER Act), entities that are required to
report their energy use and greenhouse gas emissions must report to the Australian Government. The Australian Government Clean Energy Regulator published these entities in the [National Greenhouse and Energy
Reporting Registrar](https://www.cleanenergyregulator.gov.au/NGER/National%20greenhouse%20and%20energy%20reporting%20data/Extract-of-National-Greenhouse-and-Energy-Register-by-year/national-greenhouse-and-energy-register-2021-22)

A dashboard that loads the data of the National Greenhouse and Energy Register for
2021-22 reporting year using the publicly available data.


## Getting started
Install the package first if you do not have them.

Run the python file called main.py and the dash will be running on your local server. The local server address will be shown on the message and it normally comes with http://127.0.0.1:port_number/

Click on the drop down box in the dash board to check different tables in the National Greenhouse and Energy Register 2021-22


## Installation
Using [Conda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html) or Python to install necessary environment as following commands (If not using Conda, ignore first two commands):

```
conda create -n project_name python=3.9
conda activate project_name
pip install pandas
pip install dash   
pip install dash-html-components
```

## Visuals
The dashboard would look like the following:
![alt text](img/img.png)

## License
MIT License

Copyright (c) [2023] [Ruibiao Zhu]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

